var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    nickname: String,
    hnhandle: String,
    status_email : Boolean,
    status_hn : Boolean,
    hn_token : String,
    email_token: String,
    friendslist: []
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);
