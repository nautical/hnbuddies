var passport = require('passport');
var Account = require('../models/account');
var gravatar = require('gravatar');
var mail = require("nodemailer").mail;
var requestify = require('requestify');
var path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'templates'),
    emailTemplates = require('email-templates'),
    nodemailer = require('nodemailer');
var crypto = require('crypto');

function md5(string) {
    return crypto.createHash('md5').update(string).digest('hex');
}

module.exports = function (app) {

    app.get('/', function (req, res) {
        res.render('index', {
            title: 'HN Buddies',
            author: 'Nautical',
            user: req.user,
            mess: null
        });
    });

    app.get('/register', function (req, res) {
        res.render('index', {
            title: 'HN Buddies',
            author: 'Nautical',
            user: req.user,
            mess: null
        });
    });

    app.post('/register', function (req, res) {
        var hntoken_var = (+new Date()).toString(36);
        var emailtoken_var = "123456789".split('').map(function () {
            return 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.charAt(Math.floor(62 * Math.random()));
        }).join('');
        Account.register(new Account({
            username: req.body.username,
            hnhandle: req.body.hnhandle,
            status_email: 0,
            status_hn: 0,
            hn_token: hntoken_var,
            email_token: emailtoken_var,
            friendslist: []
        }), req.body.password, function (err, account) {
            if (err) {
                res.send(err.message);
            } else {
                res.send('1');
                emailTemplates(templatesDir, function (err, template) {
                    if (err) {
                        console.log(err);
                    } else {
                        var locals = {
                            username: req.body.username,
                            link: 'http://' + req.headers.host + '/activateemail?username=' + req.body.username + '&token=' + emailtoken_var + '&val=' + md5(emailtoken_var + "j3e0DASs*a0!@"),
                            hnvar: hntoken_var
                        };
                        template('welcome', locals, function (err, html, text) {
                            mail({
                                from: "HNBuddies <team@HnBuddies.com>", // sender address
                                to: req.body.username, // list of receivers
                                subject: "Greetings from HN Buddies", // Subject line
                                html: html, // html body
                                text: text
                            });

                        });
                    };
                });
            }
        });
    });

    //  app.post('/login', passport.authenticate('local', { 
    //                                    successRedirect: '/account',
    //                                    failureRedirect: '/' }));

    app.post('/login', function (req, res, next) {
        passport.authenticate('local', function (err, user, info) {
            if (err) {
                res.send('0');
            }
            if (!user) {
                res.send('0');
            }
            req.logIn(user, function (err) {
                if (err) {
                    res.send('0');
                }
                res.send('1');
            });
        })(req, res, next);
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/activatehn', function (req, res) {
        requestify.get('https://news.ycombinator.com/user?id=' + req.user.hnhandle).then(function (response) {
            response.getBody();
            if (response.body.indexOf(req.user.hn_token) != -1) {
                Account.findOne({
                    username: req.user.username
                }, function (err, userobj) {
                    console.log(userobj);
                    if (err) {
                        res.send('0');
                    } else {
                        userobj.status_hn = 1;
                        userobj.save(function (err) {
                            if (!err) {
                                res.send('1');
                            } else {
                                res.send('0');
                            }
                        });
                    }
                });
            } else {
                res.send('0');
            }
        });
    });


    app.get('/addbuddy', function (req, res) {
        var buddymail = req.query.buddymail;
        if (req.user.status_email && req.user.status_hn) {
            Account.findOne({
                username: req.user.username
            }, function (err, userobj) {
                if (err) {
                    res.send("0");
                } else {
                    if (userobj.friendslist.indexOf(buddymail) == -1) {
                        userobj.friendslist.push(buddymail);
                        userobj.save(function (err) {
                            if (!err) {
                                emailTemplates(templatesDir, function (err, template) {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        var locals = {
                                            client: req.user.username,
                                            link: 'http://' + req.headers.host + '/activateuserbud?username=' + buddymail + '&val=' + md5(buddymail + req.user.username + "j3e0DASs*a0!@") + '&inviteby=' + req.user.username
                                        };
                                        template('invite', locals, function (err, html, text) {
                                            mail({
                                                from: "HNBuddies <team@HnBuddies.com>", // sender address
                                                to: buddymail, // list of receivers
                                                subject: "Greetings from HN Buddies", // Subject line
                                                html: html, // html body
                                                text: text
                                            });

                                        });
                                    };
                                });

                                res.send("1");
                            } else {
                                res.send("0");
                            }
                        });
                    } else {
                        res.send("0");
                    }
                }
            });
            //res.send("1");
        } else {
            console.log("both are not active");
            res.send("0");
        }
    });




    app.get('/activateemail', function (req, res) {
        //console.log(req);
        var user_email = req.query.username;
        var token = req.query.token;
        var hash = req.query.val;
        if (md5(token + "j3e0DASs*a0!@") == hash) {
            Account.findOne({
                username: user_email
            }, function (err, userobj) {
                if (err) {
                    res.render('index', {
                        title: 'HN Buddies',
                        author: 'Nautical',
                        user: '',
                        mess: 'Error in verification'
                    });
                } else {
                    if (userobj.email_token == token) {
                        userobj.status_email = 1;
                        userobj.save(function (err) {
                            if (!err) {
                                res.render('index', {
                                    title: 'HN Buddies',
                                    author: 'Nautical',
                                    user: '',
                                    mess: 'Email verified'
                                });
                            } else {
                                res.render('index', {
                                    title: 'HN Buddies',
                                    author: 'Nautical',
                                    user: '',
                                    mess: 'Error in verification'
                                });
                            }
                        });
                    } else {
                        res.render('index', {
                            title: 'HN Buddies',
                            author: 'Nautical',
                            user: '',
                            mess: 'Error in verification'
                        });
                    }
                }
            });
        } else {
            res.render('index', {
                title: 'HN Buddies',
                author: 'Nautical',
                user: '',
                mess: 'Error in verification'
            });
        }
    });

    app.get('/account', function (req, res) {
        res.render('account', {
            title: 'HN Buddies',
            author: 'Nautical',
            user: req.user,
            avatar: gravatar.url(req.user.username, {
                s: '100',
                r: 'x',
                d: 'retro'
            }, true)
        });
    });

};