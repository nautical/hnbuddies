var systemMessageInit = function(){
    	systemMessage = new systemMessage;
}

var systemMessage = function(){
	var template = "<div id='systemMessageContainer'><div id='systemMessage'></div></div>";
	var el = document.createElement("div");
	el.innerHTML = template;
	var bodyitem = document.body.firstChild;
	insertAfter(bodyitem, el);
}

systemMessage.prototype.sendMessage = function(type, message){
	var message = message;
	var type;
	
	switch(type)
	{
	case 1:
	  type = 'warning';
	  break;
	case 2:
	  type = 'error';
	  break;
	default:
	  type = '';
	}
	
	var container = document.getElementById("systemMessageContainer");
    container.style.opacity = '255';
	var messagebox = document.getElementById("systemMessage")
	messagebox.innerHTML = message;
	container.className = "revealed " + type;
	
  	setTimeout(function(){ container.className = type },4000);  	
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}